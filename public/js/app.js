console.log('Client side javascript file is loaded!')

const weatherForm = document.querySelector('form')
const search = document.querySelector('input')
const messageOne = document.querySelector('#message-1')

const icon = document.querySelector('#icon')
const temp = document.querySelector('#temp')
const desc = document.querySelector('#desc')
const address = document.querySelector('#address')

const precipitation = document.querySelector('#precipitation')
const humidity = document.querySelector('#humidity')
const wind = document.querySelector('#wind')

weatherForm.addEventListener('submit', (e) => {
    e.preventDefault()

    const location = search.value

    messageOne.textContent = 'Loading...'

    fetch_weather(location)
})

function fetch_weather(location) 
{
    fetch('/weather?address=' + location).then((response) => {
        response.json().then((data) => {
            if (data.error) {
                messageOne.textContent = data.error
            } else {
                messageOne.textContent = ''

                icon.style.backgroundImage = "url("+ data.forecast.icon +")";

                address.textContent = data.location
                temp.textContent = data.forecast.temperature + "°C"
                desc.textContent = data.forecast.description
                precipitation.textContent = data.forecast.precipitation
                humidity.textContent = data.forecast.humidity
                wind.textContent = data.forecast.windSpeed

                console.log(data.forecast)
            }
        })
    })
}

// get current location
if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(showPosition);
} else { 
    messageOne.textContent = "Geolocation is not supported by this browser."
}

function showPosition(position) {
    const location = position.coords.longitude +","+ position.coords.latitude;
    fetch_weather(location) 
}