const request = require('postman-request')

const forecast = (longitude, latitude, callback) => {
    const url = 'http://api.weatherstack.com/current?access_key=f99170d2ba808838155bfd668a0e1329&query='+ latitude +','+ longitude

    request({ url: url, json: true }, (error, response) => {
        if (error) {
            callback('Unable to connect to weather service!', undefined)
        } else if (response.body.error) {
            callback('Unable to find location', undefined)
        } else {
            callback(undefined, {
                icon: response.body.current.weather_icons[0],
                description: response.body.current.weather_descriptions[0],
                temperature: response.body.current.temperature,
                windSpeed: response.body.current.wind_speed,
                precipitation: response.body.current.precip,
                humidity: response.body.current.humidity
            })
        }
    })
}

module.exports = forecast